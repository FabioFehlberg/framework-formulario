package com.example.frameworkformulario.data;

import java.io.Serializable;
import java.util.ArrayList;

public class TratamentoDeTela implements Serializable{

    private long tempoInicial;
    private long tempoFinal;
    private int clickCount;
    private ArrayList<ClickMomento> click;

    public TratamentoDeTela(){
        clickCount = 0;
        click = new ArrayList<>();
    }

    public void setTempoInicial(long tempoInicial){
        this.tempoInicial = tempoInicial;
    }

    public void setTempoFinal(long tempoFinal){
        this.tempoFinal = tempoFinal;
    }

    public void setClick(ClickMomento click) {
        this.click.add(click);
        clickCount++;
    }

    public long getTempoInicial(){
        return tempoInicial;
    }

    public long getTempoFinal(){
        return tempoFinal;
    }

    public ClickMomento getClick(int index){
        return click.get(index);
    }

    public int getClickCount(){
        return clickCount;
    }
}
