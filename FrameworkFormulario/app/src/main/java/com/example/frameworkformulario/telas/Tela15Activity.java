package com.example.frameworkformulario.telas;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.frameworkformulario.R;
import com.example.frameworkformulario.data.Usuario;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

public class Tela15Activity extends AppCompatActivity {

    Usuario usuario;

    private FirebaseDatabase database;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tela15);

        String[] PERMISSIONS_STORAGE = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    this,
                    PERMISSIONS_STORAGE,
                    1
            );
        }

        usuario = (Usuario) getIntent().getExtras().getSerializable("usuario");

        TextView textView = (TextView) findViewById(R.id.titulo);
        textView.setTextSize(usuario.getTamanhoLetra());

        database = FirebaseDatabase.getInstance();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true); //isso permite que, mesmo offline, os dados sejam guardados e reenviados
                                                                    //assim que reconectar

        //Somente para teste
        String texto = "";
        TextView txt = (TextView) findViewById(R.id.textoTeste);
        for(int i = 0; i < usuario.getNumeroDeTelas(); i++)
            texto += "\nTela " + i + ": " + usuario.getTela(i).getClickCount() + " clicks em " + (usuario.getTela(i).getTempoFinal() - usuario.getTela(i).getTempoInicial())/1000 + " segundos";
        txt.setText("Nome: " + usuario.getNome() +" - Tela: " + usuario.getTelaWidth() + "x" + usuario.getTelaHeight() + "px - Tempo total: " + (usuario.getTela(14).getTempoFinal() - usuario.getTela(0).getTempoInicial())/1000 + " segundos" + texto);

    }


    public void proximo(View v){
        storeInFirebase();
        storeInDevice();
        Intent it = new Intent(this, IntroducaoActivity.class);
        it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //desempilha
        startActivity(it);
        finish();
    }

    public void storeInDevice(){
        if(!isExternalStorageWritable())
            return;



        // Create a file in the Internal Storage
        String uid = usuario.getNome() + usuario.getTela(0).getTempoInicial() + ".txt";

        String texto = usuario.getNome() + "\n" + usuario.getOpAnimal() + "\n" + usuario.getOpAudio() + "\n" + usuario.getOpCasa()
                + "\n" + usuario.getOpFreq() + "\n" + usuario.getOpIdade() + "\n" + usuario.getOpRS() + "\n" + usuario.getSexo()
                + "\n" + usuario.getOpCor1() + "\n" + usuario.getOpCor2() + "\n" + usuario.getOpCor3()
                + "\n" + usuario.getTamanhoLetra() + "\n" + usuario.getTelaHeight() + "\n" + usuario.getTelaWidth() + "\n";
        for(int i = 0; i < usuario.getNumeroDeTelas(); i++) {
            texto += "tela" + i + "\n" + usuario.getTela(i).getTempoInicial() + "\n" + usuario.getTela(i).getTempoFinal()
                    + "\n" + usuario.getTela(i).getClickCount() + "\n";
            for(int j = 0; j < usuario.getTela(i).getClickCount(); j++)
                texto += "evento" + j + "\n" + usuario.getTela(i).getClick(j).getX() + "\n" + usuario.getTela(i).getClick(j).getY()
                        + "\n" + usuario.getTela(i).getClick(j).getTempo() + "\n" + usuario.getTela(i).getClick(j).getAcao() + "\n";
        }

        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);
        //File folder = new File(path + "/Framework");
        File file = new File(path, uid);
        try{
            if(!path.exists())
                path.mkdirs();
            if (!file.exists())
                file.createNewFile();

            OutputStream os = new FileOutputStream(file);

            os.write(texto.getBytes());
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e("Arquivo Texto", e.getMessage(), e);
        }



    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public void storeInFirebase(){
        String uid = usuario.getNome() + "-" + usuario.getTela(0).getTempoInicial();
        mDatabase = database.getReference(uid);
        mDatabase.setValue(usuario);
        //o database não insere um objeto inteiro de uma vez, é considerado um esquema de arvore
        //abaixo a inserção de telas e clicks, sendo click filho de telas e telas filha de usuario
        for(int i = 0; i < usuario.getNumeroDeTelas(); i++) {
            mDatabase.child(String.valueOf(i)).setValue(usuario.getTela(i));
            for(int j = 0; j < usuario.getTela(i).getClickCount(); j++)
                mDatabase.child(String.valueOf(i)).child(String.valueOf(j)).setValue(usuario.getTela(i).getClick(j));
        }
    }

}