package com.example.frameworkformulario.telas;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.frameworkformulario.R;
import com.example.frameworkformulario.data.Usuario;

public class Tela14Activity extends AppCompatActivity {

    Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tela14);

        usuario = (Usuario) getIntent().getExtras().getSerializable("usuario");

        TextView textView = (TextView) findViewById(R.id.titulo);
        textView.setTextSize(usuario.getTamanhoLetra());


    }


    public void proximo(View v){
        Intent it = new Intent(this, Tela15Activity.class);
        it.putExtra("usuario", usuario);
        startActivity(it);
    }
}
