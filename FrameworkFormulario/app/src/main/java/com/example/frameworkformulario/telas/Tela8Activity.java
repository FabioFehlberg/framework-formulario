package com.example.frameworkformulario.telas;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.frameworkformulario.R;
import com.example.frameworkformulario.data.ClickMomento;
import com.example.frameworkformulario.data.Usuario;

import static android.view.MotionEvent.ACTION_CANCEL;


public class Tela8Activity extends AppCompatActivity {

    private int TELA_ATUAL = 11;

    Usuario usuario;

    ImageView lixeira;
    ImageView iconeAndroid;

    boolean soltouDeleta;
    boolean deletou;

    public float iconeAndroid_height;
    public float iconeAndroid_width;
    public Rect rectIcone;
    public Rect rectLixeira;

    private float iconeX;
    private float iconeY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tela8);

        usuario = (Usuario) getIntent().getExtras().getSerializable("usuario");
        //TELA_ATUAL = getIntent().getExtras().getInt("TELA_ATUAL");
        usuario.getTela(TELA_ATUAL).setTempoInicial(System.currentTimeMillis());

        TextView textView = (TextView) findViewById(R.id.titulo);
        textView.setTextSize(usuario.getTamanhoLetra());

        lixeira = (ImageView) findViewById(R.id.lixeira);
        iconeAndroid = (ImageView) findViewById(R.id.iconeAndroid);

        soltouDeleta = false;
        deletou = false;


    }

    @Override
    protected void onResume() {
        super.onResume();
        iconeAndroid.post(new Runnable() {
            @Override
            public void run() {
                iconeAndroid_height = iconeAndroid.getHeight();
                iconeAndroid_width = iconeAndroid.getWidth();
                iconeX = iconeAndroid.getX();
                iconeY = iconeAndroid.getY();
                configuraTouch();
            }
        });
    }

    public void configuraTouch() {
        iconeAndroid.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent m) {
                float x = m.getRawX();
                float y = m.getRawY() - 100;

                switch (m.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        usuario.getTela(TELA_ATUAL).setClick(new ClickMomento(m.getX(),m.getY(),System.currentTimeMillis(),"ACTION_DOWN"));
                        deletou = true;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        return true;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_POINTER_UP:
                        if(soltouDeleta){
                            iconeAndroid.setClickable(false);
                            iconeAndroid.setVisibility(View.INVISIBLE);
                            //deletou = true;
                        }
                        else {
                            iconeAndroid.setX(iconeX);
                            iconeAndroid.setY(iconeY);
                        }
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        usuario.getTela(TELA_ATUAL).setClick(new ClickMomento(m.getX(),m.getY(),System.currentTimeMillis(),"ACTION_MOVE"));
                        //tratamento para arrastar imagem
                        iconeAndroid.setX(((int) x) - (iconeAndroid_width / 2));
                        iconeAndroid.setY(((int) y) - (iconeAndroid_height / 2));

                        //caso a imagem colida com a outra - https://developer.android.com/reference/android/graphics/Rect.html#intersects%28android.graphics.Rect,%20android.graphics.Rect%29
                        rectIcone = new Rect();
                        iconeAndroid.getHitRect(rectIcone);
                        rectLixeira = new Rect();
                        lixeira.getHitRect(rectLixeira);
                        if (Rect.intersects(rectIcone, rectLixeira)) {
                            //iconeAndroid.setVisibility(View.INVISIBLE);
                            //iconeAndroid.setClickable(false);
                            lixeira.setBackgroundColor(Color.GREEN);
                            soltouDeleta = true;
                        } else {
                            //iconeAndroid.setVisibility(View.VISIBLE);
                            //iconeAndroid.setClickable(true);
                            lixeira.setBackgroundColor(Color.TRANSPARENT);
                            soltouDeleta = false;
                        }
                        return true;
                }
                return false;
            }
        });
    }

    public void proximo(View v){

        if(deletou){
            Intent it = new Intent(this, Tela9Activity.class);
            usuario.getTela(TELA_ATUAL).setTempoFinal(System.currentTimeMillis());
            //it.putExtra("TELA_ATUAL", TELA_ATUAL+1);
            it.putExtra("usuario", usuario);
            startActivity(it);
            //finish();
        }
        else
            Toast.makeText(this, "Tente deletar antes de prosseguir.", Toast.LENGTH_SHORT).show();
    }

    //http://www.vogella.com/tutorials/AndroidTouch/article.html - MultiTouch  -  o resto é Android Developer
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev){
        // get pointer index from the event object
        int pointerIndex = ev.getActionIndex();

        // get pointer ID
        int pointerId = ev.getPointerId(pointerIndex);

        // get masked (not specific to a pointer) action
        int maskedAction = ev.getActionMasked();

        switch (maskedAction) {

            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {
                // We have a new pointer. Lets add it to the list of pointers
                //Log.i("TESTE", "ACTION_POINTER_DOWN - X :" + ev.getX() + " Y :" + ev.getY());
                usuario.getTela(TELA_ATUAL).setClick(new ClickMomento(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_DOWN"));
                break;
            }
            case MotionEvent.ACTION_MOVE: { // a pointer was moved
                for (int size = ev.getPointerCount(), i = 0; i < size; i++) {
                    //PointF point = mActivePointers.get(ev.getPointerId(i));
                    //Log.i("TESTE", i + "ACTION_MOVE - X :" + ev.getX(i) + " Y :" + ev.getY(i));
                    usuario.getTela(TELA_ATUAL).setClick(new ClickMomento(ev.getX(i),ev.getY(i),System.currentTimeMillis(),"ACTION_MOVE"));
                }
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
            case ACTION_CANCEL: {
                //Log.i("TESTE", "ACTION_CANCEL - X :" + ev.getX() + " Y :" + ev.getY());
                usuario.getTela(TELA_ATUAL).setClick(new ClickMomento(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_CANCEL"));
                break;
            }
        }

        return super.dispatchTouchEvent(ev);
    }

}
