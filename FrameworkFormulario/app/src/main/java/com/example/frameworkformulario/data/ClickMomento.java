package com.example.frameworkformulario.data;


import java.io.Serializable;

public class ClickMomento implements Serializable{

    private float X;
    private float Y;
    private long tempo;
    private String acao;

    public ClickMomento(){}

    public ClickMomento(float X, float Y, long tempo, String acao){
        this.tempo = tempo;
        this.X = X;
        this.Y = Y;
        this.acao = acao;
    }

    public void setX(float X){
        this.X = X;
    }

    public void setY(float Y){
        this.Y = Y;
    }

    public void setTempo(long tempo){
        this.tempo = tempo;
    }

    public float getX(){
        return X;
    }

    public float getY(){
        return Y;
    }

    public long getTempo() {
        return tempo;
    }

    public void setAcao(String acao){
        this.acao = acao;
    }

    public String getAcao(){
        return acao;
    }
}
