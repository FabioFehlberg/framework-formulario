package com.example.frameworkformulario.telas;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.frameworkformulario.R;
import com.example.frameworkformulario.data.ClickMomento;
import com.example.frameworkformulario.data.Usuario;

import static android.view.MotionEvent.ACTION_CANCEL;

public class Tela2SActivity extends AppCompatActivity {

    private int TELA_ATUAL = 3;

    Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tela2_s);

        usuario = (Usuario) getIntent().getExtras().getSerializable("usuario");
        //TELA_ATUAL = getIntent().getExtras().getInt("TELA_ATUAL");
        usuario.getTela(TELA_ATUAL).setTempoInicial(System.currentTimeMillis());

        TextView textView = (TextView) findViewById(R.id.titulo);
        textView.setTextSize(usuario.getTamanhoLetra());

    }

    public void sexoClick(View v){
        ImageView outraImagem;

        if(Integer.parseInt(v.getTag().toString()) == 1){
            usuario.setSexo("masculino");
            v.setBackgroundColor(Color.GREEN);
            outraImagem = (ImageView) findViewById(R.id.fImage);
            outraImagem.setBackgroundColor(Color.TRANSPARENT);
        }
        else{
            usuario.setSexo("feminino");
            v.setBackgroundColor(Color.GREEN);
            outraImagem = (ImageView) findViewById(R.id.mImage);
            outraImagem.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    public void proximo(View v){
        if(!usuario.getSexo().equals("")){
            Intent it = new Intent(this, Tela2Activity.class);
            usuario.getTela(TELA_ATUAL).setTempoFinal(System.currentTimeMillis());
            //it.putExtra("TELA_ATUAL", TELA_ATUAL+1);
            it.putExtra("usuario",usuario);
            startActivity(it);
        }
        else
            Toast.makeText(this,"Favor selecionar o seu sexo para poder prosseguir",Toast.LENGTH_SHORT).show();
    }

    //http://www.vogella.com/tutorials/AndroidTouch/article.html - MultiTouch  -  o resto é Android Developer
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev){
        // get pointer index from the event object
        int pointerIndex = ev.getActionIndex();

        // get pointer ID
        int pointerId = ev.getPointerId(pointerIndex);

        // get masked (not specific to a pointer) action
        int maskedAction = ev.getActionMasked();

        switch (maskedAction) {

            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {
                // We have a new pointer. Lets add it to the list of pointers
                //Log.i("TESTE", "ACTION_POINTER_DOWN - X :" + ev.getX() + " Y :" + ev.getY());
                usuario.getTela(TELA_ATUAL).setClick(new ClickMomento(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_DOWN"));
                break;
            }
            case MotionEvent.ACTION_MOVE: { // a pointer was moved
                for (int size = ev.getPointerCount(), i = 0; i < size; i++) {
                    //PointF point = mActivePointers.get(ev.getPointerId(i));
                    //Log.i("TESTE", i + "ACTION_MOVE - X :" + ev.getX(i) + " Y :" + ev.getY(i));
                    usuario.getTela(TELA_ATUAL).setClick(new ClickMomento(ev.getX(i),ev.getY(i),System.currentTimeMillis(),"ACTION_MOVE"));
                }
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
            case ACTION_CANCEL: {
                //Log.i("TESTE", "ACTION_CANCEL - X :" + ev.getX() + " Y :" + ev.getY());
                usuario.getTela(TELA_ATUAL).setClick(new ClickMomento(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_CANCEL"));
                break;
            }
        }

        return super.dispatchTouchEvent(ev);
    }
}
