package com.example.frameworkformulario.telas;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.frameworkformulario.R;
import com.example.frameworkformulario.data.ClickMomento;
import com.example.frameworkformulario.data.Usuario;
import com.github.chrisbanes.photoview.PhotoView;

import static android.view.MotionEvent.ACTION_CANCEL;

public class Tela5Activity extends AppCompatActivity {

    private int TELA_ATUAL = 6;

    Usuario usuario;

    PhotoView cachorro;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tela5);

        usuario = (Usuario) getIntent().getExtras().getSerializable("usuario");
        //TELA_ATUAL = getIntent().getExtras().getInt("TELA_ATUAL");
        usuario.getTela(TELA_ATUAL).setTempoInicial(System.currentTimeMillis());

        TextView textView = (TextView) findViewById(R.id.titulo);
        textView.setTextSize(usuario.getTamanhoLetra());

        cachorro = (PhotoView) findViewById(R.id.cachorro);
        cachorro.setMinimumScale(0);
        cachorro.setImageResource(R.drawable.cachorro);
        cachorro.setScaleType(ImageView.ScaleType.CENTER);
        cachorro.getAttacher().setScale(0.4489f,true);
        cachorro.setZoomable(true);

        RadioButton r = (RadioButton) findViewById(R.id.animal1);
        r.setTextSize(usuario.getTamanhoLetra());
        r = (RadioButton) findViewById(R.id.animal2);
        r.setTextSize(usuario.getTamanhoLetra());
        r = (RadioButton) findViewById(R.id.animal3);
        r.setTextSize(usuario.getTamanhoLetra());
        r = (RadioButton) findViewById(R.id.animal4);
        r.setTextSize(usuario.getTamanhoLetra());

    }

    @Override
    protected void onResume(){
        super.onResume();

    }

    //https://www.youtube.com/watch?v=plnLs6aST1M - acessado 25/05
    public void gifButton(View v){

        AlertDialog.Builder builder = new AlertDialog.Builder(Tela5Activity.this);
        View view = getLayoutInflater().inflate(R.layout.dialog_zoom, null);

        WebView webGif = (WebView) view.findViewById(R.id.gifZoom);
        webGif.setBackgroundColor(Color.TRANSPARENT);
        webGif.loadUrl("file:///android_res/drawable/zoom.gif");

        Button fecharBt = (Button) view.findViewById(R.id.fecharDialog);

        builder.setView(view);
        final AlertDialog dialog = builder.create();
        dialog.show();

        fecharBt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog.dismiss();
            }
        });
    }

    public void animalClick(View v){
        switch (Integer.parseInt(v.getTag().toString())){
            case 1:
                usuario.setOpAnimal("gato");
                break;
            case 2:
                usuario.setOpAnimal("cachorro");
                break;
            case 3:
                usuario.setOpAnimal("rato");
                break;
            case 4:
                usuario.setOpAnimal("leao");
                break;
        }
    }

    public void proximo(View v){
        if(!usuario.getOpAnimal().equals("")){
            Intent it = new Intent(this, Tela6Activity.class);
            usuario.getTela(TELA_ATUAL).setTempoFinal(System.currentTimeMillis());
            it.putExtra("usuario", usuario);
            startActivity(it);
            //finish();
        }
        else
            Toast.makeText(this, "Selecione uma opção para prosseguir.", Toast.LENGTH_SHORT).show();
    }

    //http://www.vogella.com/tutorials/AndroidTouch/article.html - MultiTouch  -  o resto é Android Developer
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev){
        // get pointer index from the event object
        int pointerIndex = ev.getActionIndex();

        // get pointer ID
        int pointerId = ev.getPointerId(pointerIndex);

        // get masked (not specific to a pointer) action
        int maskedAction = ev.getActionMasked();

        switch (maskedAction) {

            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {
                // We have a new pointer. Lets add it to the list of pointers
                //Log.i("TESTE", "ACTION_POINTER_DOWN - X :" + ev.getX() + " Y :" + ev.getY());
                usuario.getTela(TELA_ATUAL).setClick(new ClickMomento(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_DOWN"));
                break;
            }
            case MotionEvent.ACTION_MOVE: { // a pointer was moved
                for (int size = ev.getPointerCount(), i = 0; i < size; i++) {
                    //PointF point = mActivePointers.get(ev.getPointerId(i));
                    //Log.i("TESTE", i + "ACTION_MOVE - X :" + ev.getX(i) + " Y :" + ev.getY(i));
                    usuario.getTela(TELA_ATUAL).setClick(new ClickMomento(ev.getX(i),ev.getY(i),System.currentTimeMillis(),"ACTION_MOVE"));
                }
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
            case ACTION_CANCEL: {
                //Log.i("TESTE", "ACTION_CANCEL - X :" + ev.getX() + " Y :" + ev.getY());
                usuario.getTela(TELA_ATUAL).setClick(new ClickMomento(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_CANCEL"));
                break;
            }
        }

        return super.dispatchTouchEvent(ev);
    }

}
