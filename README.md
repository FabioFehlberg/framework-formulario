# README #

Como discutido com a Dâmaris na reunião de quinta-feira(18/05), vou implementar somente a funcionalidade das telas
sem que o aplicativo gere qualquer dado como retorno. Chegamos a esta conclusão pois encontrei alguns obstaculos 
enquanto implementava as telas juntamente com a obtenção e o retorno dos dados necessários e deixamos esta parte de 
lado por enquanto para termos todas as telas do formulário implementadas e funcionais a tempo. Caso, até a data de 
apresentação do aplicativo eu não tenha conseguido implementar estas operações vamos seguir com a sugestão do professor
Lucas Vegi e usar o aplicativo "AZ Screen Recorder" para gravas as sessões e daí analisar os dados.

### What is this repository for? ###

Este repositório servirá como ponto de acompanhamento do desenvolvimento do aplicativo que será utilizado nos testes
de levantamento de dados para implementar o Framework sugerido pela Dâmaris Arruda.

### Who do I talk to? ###

Fábio Chaves Fehlberg
fabio.chaves.f@gmail.com